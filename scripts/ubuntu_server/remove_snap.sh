#!/usr/bin/env bash

set -e

# Remove all snap packages
for $pkg in $(snap list | awk '{print $1}' | tail -n +2); do
	sudo snap remove --purge $pkg
done
sudo snap remove --purge core18
sudo snap remove --purge snapd

# Remove the snap cache
[ -e /var/cache/snapd ] && sudo rm -rf /var/cache/snapd/

# Remove snap
sudo apt autoremove --purge snapd

# Remove snap preferences
[ -e $HOME/snap ] && rm -rf $HOME/snap

# Mark snap as 'hold'
sudo apt-mark hold snapd
