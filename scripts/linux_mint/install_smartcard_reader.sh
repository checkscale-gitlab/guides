#!/usr/bin/env bash

set -e

# Force the user to close the browsers
echo "CLOSE ALL OF YOUR BROWSERS BEFORE PROCEDING"
echo -n "When you're sure you've closed all of your browsers, press Enter "
read foo

# Install OpenSC and PC/SC
sudo apt install -y pcscd opensc libnss3-tools libccid

# Install the drivers for the 'ACS USB CCID' smart card readers
# (https://wiki.debian.org/Smartcards#ACS_USB_CCID_smart_card_readers)
sudo apt install -y libacsccid1

# Configure the OpenSC module for the browsers
modutil -force -dbdir sql:$HOME/.pki/nssdb/ -add "OpenSC" -libfile /usr/lib/$(uname -i)-linux-gnu/opensc-pkcs11.so
