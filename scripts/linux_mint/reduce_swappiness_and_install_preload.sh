#!/usr/bin/env bash

set -e

# Install 'preload'
echo -ne "\nDo you want to install preload? [y/N] "
read foo
if [[ $foo =~ ^[yY]$ ]]; then
	sudo apt install -y preload
fi

# Reduce the swappiness
echo -ne '\n# Reduce the swappiness\nvm.swappiness=10' | sudo tee -a /etc/sysctl.conf

echo -e "\nYou have to reboot to apply the changes!"
