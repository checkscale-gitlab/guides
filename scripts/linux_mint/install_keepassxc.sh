#!/usr/bin/env bash

set -e

# Add KeePassXC repository
sudo add-apt-repository -y ppa:phoerious/keepassxc
sudo apt update

# Install KeePassXC
sudo apt install -y keepassxc
