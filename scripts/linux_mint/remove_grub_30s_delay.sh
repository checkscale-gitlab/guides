#!/usr/bin/env bash

set -e

# Remove the 30s timeout after an update
echo -e '\n# Remove the 30s timeout after an update\nGRUB_RECORDFAIL_TIMEOUT=0' | sudo tee -a /etc/default/grub
sudo update-grub

echo -e "\nYou have to reboot to apply the changes!"
