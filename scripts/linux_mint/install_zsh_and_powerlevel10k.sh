#!/usr/bin/env bash

set -e

# Install ZSH
sudo apt install -y zsh zsh-doc

# Change the shell for the user
chsh -s /usr/bin/zsh

# Install Oh-My-ZSH
curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh | bash

# Install Powerlevel10k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $HOME/.oh-my-zsh/custom/themes/powerlevel10k
sed -i 's,ZSH_THEME="robbyrussell",ZSH_THEME="powerlevel10k/powerlevel10k",g' "$HOME/.zshrc"

# Remove some dotfiles
[ -e $HOME/.sudo_as_admin_successful ] && rm $HOME/.sudo_as_admin_successful
rm $HOME/.bash*

# Install some plugins
git clone --depth=1 https://github.com/zsh-users/zsh-autosuggestions $HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions
git clone --depth=1 https://github.com/zsh-users/zsh-syntax-highlighting.git $HOME/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
sed -i "s,plugins=(git),plugins=(\n\tgit\n\tzsh-autosuggestions\n\tzsh-syntax-highlighting\n),g" "$HOME/.zshrc"

echo -e "\nYou have to log out and log back in to apply the changes!"
