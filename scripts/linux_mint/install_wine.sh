#!/usr/bin/env bash

set -e

# Enable 32 bit architecture
sudo dpkg --add-architecture i386

# Add Wine repository
curl -fsSL "https://dl.winehq.org/wine-builds/winehq.key" | sudo apt-key add -
sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'
sudo apt update

# Install Docker
sudo apt install -y --install-recommends winehq-stable winetricks
